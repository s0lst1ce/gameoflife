/*
 * <Game of Life, simple game of life board>
 * Copyright (C) 2020  GtN gtn@tutanota.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "case_object.h"

Case_object::Case_object(){
    state = 0;
    neighbours = 0;
    neighbours_link = std::vector<Case_object *>();
    shape = sf::RectangleShape();
    posX = 0; posY = 0;
}


Case_object::Case_object(case_state state_, int posX_, int posY_, int size) {
    neighbours = 0;
    state = state_;
    neighbours_link = std::vector<Case_object *>();
    
//     text_link_black = text_link_black_;
//     text_link_white = text_link_white_;
//     
//     if(state_ == 0)
//         sprite.setTexture(* text_link_black_);
//     else
//         sprite.setTexture(* text_link_white_);
    
    shape = sf::RectangleShape();
    
    shape.setSize({static_cast<float>(size), static_cast<float>(size)});
    
    if(state == 0)
        shape.setFillColor(sf::Color::Black);
    else 
        shape.setFillColor(sf::Color::White);
    
    
    posX = posX_;
    posY = posY_;
    shape.setPosition(posX, posY);
}

void Case_object::send_signal() {
    if(state == 0) {
        for(unsigned short int i(0); i < neighbours_link.size(); i++) {
            (neighbours_link[i]->neighbours)++;                
        }
    }
}

void Case_object::set_state(case_state state_) {
    state = state_;
    if(state == 0)
        shape.setFillColor(sf::Color::Black);
    else 
        shape.setFillColor(sf::Color::White);
}

 void Case_object::update_case() {
    if(state == 0) {
        if(neighbours != 2 && neighbours != 3) {
            set_state(1);
        }
    } else {
        if(neighbours == 3) {
            set_state(0);
        }
    }
    neighbours = 0;
}
