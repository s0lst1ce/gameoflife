
/*
 * <Game of Life, simple game of life board>
 * Copyright (C) 2020  GtN gtn@tutanota.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CASE_OBJECT_H
#define CASE_OBJECT_H

#include <bitset>
#include <vector>
#include <iostream>
#include <SFML/Graphics.hpp>

typedef std::bitset<1> case_state;

/**
 * @todo write docs
 */
class Case_object
{
public:
    /**
     * Default constructor
     */
    Case_object();
    Case_object(case_state state_, int posX_, int posY_, int size);
    void update_case();
    void set_state(case_state state_);
    void send_signal();

    case_state state;
    std::vector<Case_object *> neighbours_link;
    int neighbours;
    int posX, posY;
    sf::RectangleShape shape;
};

#endif // CASE_OBJECT_H
