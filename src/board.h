/*
 * <Game of Life, simple game of life board>
 * Copyright (C) 2020  GtN gtn@tutanota.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOARD_H
#define BOARD_H

#include <bitset>
#include <vector>
#include <ctime>
#include <iostream>
#include <iterator>
// #include <SFML/Graphics.hpp>

#include "case_object.h"

// #ifndef INIT_ERROR
//     #define INIT_ERROR = EXIT_FAILURE;
// #endif
/**
 * @todo write docs
 */
class Board
{
public:
    /**
     * Default constructor
     */
    Board();
    ~Board();
    void init_board(unsigned short int const width = 10, unsigned short int const height = 10, unsigned short int const black_percent = 50, unsigned short const int case_size = 10);
    void show_board(unsigned short int const mode);
    void update();
    int get_error_state();
    void set_state(unsigned long int const x, unsigned long const int y, case_state const state);
    unsigned long int get_x_size();
    unsigned long int get_y_size();
    
    void clear();
    
    sf::RectangleShape * get_sprite(unsigned long int const x, unsigned long int const y);
    bool is_in_board(unsigned long int const x, unsigned long int const y);
    
    case_state get_state(unsigned long int const x, unsigned long int const y);
    
private:
    std::vector<std::vector<Case_object>> board_vect;
//     sf::Texture * texture_white, * texture_black;
    int error_state, m_case_size;
    

};

#endif // BOARD_H
