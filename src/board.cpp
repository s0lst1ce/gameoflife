/*
 * <Game of Life, simple game of life board>
 * Copyright (C) 2020  GtN gtn@tutanota.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "board.h"

Board::Board() {
    board_vect = std::vector<std::vector<Case_object>>();
    error_state = 0;
    m_case_size = 10;
}

Board::~Board() {}

int Board::get_error_state() { return error_state; }

unsigned long int  Board::get_x_size() { return board_vect.size(); }
unsigned long int  Board::get_y_size() { return  (board_vect.size() > 0) ? board_vect[0].size() : 0; }

bool Board::is_in_board(unsigned long int const x, unsigned long int const y) {
    return (x < this->get_x_size() && y < this->get_y_size());
}

sf::RectangleShape * Board::get_sprite(unsigned long int const x, unsigned long int const y) {
    if(is_in_board(x, y)) {
        return &board_vect[x][y].shape;
    } else {
        return new sf::RectangleShape();
    }
}

void Board::init_board(unsigned short const int width, unsigned short const int height, unsigned short int const black_percent, unsigned short const int case_size) {
    srand(time(0));
    
    m_case_size = case_size;
    
    for(int i(0); i < width; i++) {
        board_vect.push_back(std::vector<Case_object>());
        for(int j(0); j < height; j++) {
            if(rand() % 100 < black_percent)
                board_vect[i].push_back(Case_object(0, i*m_case_size, j*m_case_size, m_case_size)); // 0b0 -> Black
            else
                board_vect[i].push_back(Case_object(1, i*m_case_size, j*m_case_size, m_case_size)); // 0b1 -> white
        }
    }

    // Make links
    for(unsigned long int i(0); i < board_vect.size(); i++) {
        for(unsigned long int j(0); j < board_vect[i].size(); j++) {
            if(i != 0) {
                board_vect[i][j].neighbours_link.push_back(&board_vect[i - 1][j]);
            }
            if(i != 0 && j != 0) {
                board_vect[i][j].neighbours_link.push_back(&board_vect[i - 1][j -1]);
            }
            if(j != 0) {
                board_vect[i][j].neighbours_link.push_back(&board_vect[i][j - 1]);
            }
            if(j != 0 && i != board_vect.size() - 1) {
                board_vect[i][j].neighbours_link.push_back(&board_vect[i + 1][j - 1]);
            }
            if(i != board_vect.size() - 1) {
                board_vect[i][j].neighbours_link.push_back(&board_vect[i + 1][j]);
            }
            if(i != board_vect.size() - 1 && j != board_vect.size() - 1) {
                board_vect[i][j].neighbours_link.push_back(&board_vect[i + 1][j + 1]);
            }
            if(j != board_vect.size() - 1) {
                board_vect[i][j].neighbours_link.push_back(&board_vect[i][j + 1]);
            }
            if(i!= 0 && j != board_vect.size() - 1) {
                board_vect[i][j].neighbours_link.push_back(&board_vect[i - 1][j + 1]);
            }
        }
    }    
}

void Board::show_board(unsigned short int const mode) {
    if(mode == 0) {
        for(auto a:board_vect) {
            for(auto b:a) {
                if(b.state == 1) {
                    std::cout << "█";
                } else {
                    std::cout << "▒";
                }
            }
            std::cout << std::endl;
        }
    }
}

void Board::update() {
    for(unsigned long int i(0); i < board_vect.size(); i++) {
        for(unsigned long int j(0); j < board_vect[0].size(); j++) {
            board_vect[i][j].send_signal();
        }
    }    
    for(unsigned long int i(0); i < board_vect.size(); i++) {
        for(unsigned long int j(0); j < board_vect[0].size(); j++) {
            board_vect[i][j].update_case();
        }
    }
}
void Board::set_state(unsigned long int const  x, unsigned long int const y, case_state const state) {
    if(is_in_board(x, y)) {
        board_vect[x][y].set_state(state);
    }
}
case_state Board::get_state(unsigned long int const  x, unsigned long int const y) {
    return is_in_board(x, y) ? board_vect[x][y].state : 0;
}

void Board::clear() {    
    for(unsigned long int i(0); i < board_vect.size(); i++) {
        for(unsigned long int j(0); j < board_vect[0].size(); j++) {
            board_vect[i][j].set_state(1);
        }
    }
}
